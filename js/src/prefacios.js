var gallery;

// Detecção de scroll para fechar as exibições fullscreen
window.onscroll = function(e) {
    // Talvez seja necessario checar a largura da tela para prevenir fechamentos (rs) acidentais em telas touch
    if(this.oldScroll < this.scrollY)
    {
        if($("#projeto-video-wrapper").is(":visible"))
        {
            player.stopVideo();
            $("#projeto-video-wrapper").fadeOut(150);
        }
    }
    this.oldScroll = this.scrollY;

    if($(window).scrollTop()>=$('#content').position().top + 10)
    {
        $(".hamburger").addClass("scrolled");
        $(".navbar > .collapse").addClass("scrolled");
    }
    else
    {
        $(".hamburger").removeClass("scrolled");
        $(".navbar > .collapse").removeClass("scrolled");
    }
}

$('*').keypress(function(e){
    if (e.which == 27) {
        if($("#projeto-video-wrapper").is(":visible"))
        {
            player.stopVideo();
            $("#projeto-video-wrapper").fadeOut(150);
        }
    }
});

// Preloader
$(window).on('load', function() { // makes sure the whole site is loaded
  $('#loading-icon').fadeOut();
  $('#loading').delay(350).fadeOut('slow');
  //$('#navbarNav').fadeIn('fast')
  $('body').delay(350).css({'overflow':'visible'});
});

// ImagesLoaded
$('#depoimentos').imagesLoaded()
  .always( function( instance ) {
    $('#depoimentos').fadeIn();
});

$('#livros').imagesLoaded()
  .always( function( instance ) {
    $('#livros').fadeIn();
});

// Temporariamente desativado [Victor]
/*$('#repercussao img').imagesLoaded()
  .always( function( instance ) {
    $(this).fadeIn();
});*/

// Atribui o arquivo de vídeo mais adequado à resolução da tela. [Victor]
function updateIntroVideo()
{
    var w = $(window).width();
    var h = $(window).height();
    var basefile = "video/intro_";

    if(h >= 1080)
        file = basefile + "1080p.mp4";
    else if(h >= 720)
        file = basefile + "1080p.mp4"; // Suspeito que o problema no autoplay seja com o arquivo 720p. Temporário [Victor]
    else if(h >= 480)
        file = basefile + "480p.mp4";
    else if(h >= 360)
        file = basefile + "360p.mp4";
    else
        file = basefile + "180p.mp4";

    $("#video-source").attr("src", file);
    $("#video-bg")[0].load();

    return;
}

$( window ).resize(function() {
  //updateIntroVideo(); // Isso resultaria no download de múltiplos arquivos se a resolução mudar. O que pode ser um problema em conexões com franquia de dados. [Victor]
});

$(document).ready(function()
{
    //$('#navbarNav').hide();
    
    $('.fluidbox-trigger').fluidbox({
        immediateOpen: true,
        loader: true
    });

    // Esconde os containeres para exibi-los apenas quando as imagens carregarem [Victor]
    $('#depoimentos').hide();
    $('#livros').hide();
    //$('#repercussao img').hide(); Temporariamente desativado [Victor]

    updateIntroVideo();

    //$("#projeto-video-wrapper").hide();
    // Featherlight Lightbox
    /*
    gallery = $('.gallery').featherlightGallery({
        gallery: {
            fadeIn: 300,
            fadeOut: 300
        },
        openSpeed: 300,
        closeSpeed: 300
    });
    */

    // Fechar menu responsivo ao clicar em um link
    $('.navbar-nav>li>a').on('click', function() {
        $('.navbar-collapse').collapse('hide');
    });

    // Hash Nav
    // Select all links with hashes
    $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 300, function() {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });

    // Não pode arrastar o conteúdo
    $('img').on('dragstart', function(event) {
        event.preventDefault();
    });
    $('a').on('dragstart', function(event) {
        event.preventDefault();
    });

    // Sliders
    $('#owl-livros').owlCarousel({
        margin: 15,
        nav: true,
        navText: ['❮', '❯'],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });
    $('#owl-midia').owlCarousel({
        margin: 15,
        nav: true,
        navText: ['❮', '❯'],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
});


(function($) {
    /**
     * Copyright 2012, Digital Fusion
     * Licensed under the MIT license.
     * http://teamdf.com/jquery-plugins/license/
     *
     * @author Sam Sehnert
     * @desc A small plugin that checks whether elements are within
     *     the user visible viewport of a web browser.
     *     only accounts for vertical position, not horizontal.
     */

    $.fn.visible = function(partial) {

        var $t = $(this),
            $w = $(window),
            viewTop = $w.scrollTop(),
            viewBottom = viewTop + $w.height(),
            _top = $t.offset().top,
            _bottom = _top + $t.height(),
            compareTop = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

    };

})(jQuery);

var win = $(window);

var allMods = $(".module");

allMods.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
        el.addClass("already-visible");
    }
});

win.scroll(function(event) {

    allMods.each(function(i, el) {
        var el = $(el);
        if (el.visible(true) && !$(el).hasClass("already-visible")) {
            el.addClass("come-in");
        }
    });

});

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  event.target.playVideo();
}

// Controla a visibilidade do vídeo
var trigger = document.querySelectorAll(".projeto-video");
for (var i = 0; i < trigger.length; i++)
{
  trigger[i].addEventListener("click", function()
  {
    player.playVideo();
    $("#projeto-video-wrapper").fadeIn(150);
  });
};

$('#projeto-video-wrapper .close').on('click', function(event) {
  player.stopVideo();
  $("#projeto-video-wrapper").fadeOut(150);
});

// Lazy-load Depoimentos do YouTube
(function()
{
  var youtube = document.querySelectorAll( ".youtube" );
  for (var i = 0; i < youtube.length; i++)
  {
    var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
    var image = new Image();
    image.src = source;
    image.className = "video-thumb-img";
    image.addEventListener("load", function()
    {
      youtube[i].appendChild(image);
    }(i));
    youtube[i].addEventListener( "click", function()
    {
      console.log("Clicado data-embed "+this.dataset.embed);
      var iframe = document.createElement( "iframe" );
      iframe.setAttribute( "frameborder", "0" );
      iframe.setAttribute( "allowfullscreen", "" );
      iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );
      this.innerHTML = "";
      this.appendChild( iframe );
    });
  };
})();

