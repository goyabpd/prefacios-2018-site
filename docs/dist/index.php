<?php
/**
 * This example shows how to handle a simple contact form.
 */

  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

$msg = '';
//Don't run this unless we're handling a form submission
if (array_key_exists('email', $_POST)) {
    date_default_timezone_set('Etc/UTC');

    require 'vendor/phpmailer/src/Exception.php';
    require 'vendor/phpmailer/src/PHPMailer.php';
    require 'vendor/phpmailer/src/SMTP.php';

    //Create a new PHPMailer instance
    $mail = new PHPMailer;
    //Tell PHPMailer to use SMTP - requires a local mail server
    //Faster and safer than using mail()
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 25;
    $mail->Username   = "prefaciodehistorias@gmail.com"; // SMTP account username
    $mail->Password   = "pr3f4c10";     // SMTP account password
    $mail->SMTPAuth = true;

    //Use a fixed address in your own domain as the from address
    //**DO NOT** use the submitter's address here as it will be forgery
    //and will cause your messages to fail SPF checks
    $mail->setFrom('victorgago1607@gmail.com', 'Formulário de Contato');
    //Send the message to yourself, or whoever should receive contact for submissions
    $mail->addAddress('laganjamecanica@gmail.com', 'Prefácio de Histórias');
    //Put the submitter's address in a reply-to header
    //This will fail if the address provided is invalid,
    //in which case we should ignore the whole request
    if ($mail->addReplyTo($_POST['email'], $_POST['name'])) {
        $mail->Subject = 'Formulário de contato em Prefácio de Histórias';
        //Keep it simple - don't use HTML
        $mail->isHTML(false);
        //Build a simple message body
        $mail->Body = <<<EOT
Email: {$_POST['email']}
Nome: {$_POST['name']}
Mensagem: {$_POST['message']}
EOT;
        //Send the message, check for errors
        if (!$mail->send()) {
            //The reason for failing to send will be in $mail->ErrorInfo
            //but you shouldn't display errors to users - process the error, log it on your server.
            $msg = 'Desculpe, ocorreu um erro ao processar seu pedido. Por favor, tente novamente.'.$mail->ErrorInfo;
        } else {
            $msg = 'Mensagem enviada.';
        }
    } else {
        $msg = 'Por favor, verifique o e-mail informado e tente novamente.';
    }
}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118623816-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-118623816-1');
    </script>

    <!-- Links -->
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <title>Prefácio de Histórias</title>
    <meta name=”description” content="Começos de livros que contam recomeços de vidas.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  </head>
  <body>

    <!-- Tela de carregamento -->
    <div id="loading">
      <div class="loading-container">
        <div id="loading-icon" class="book row block">
          <div class="book__page"></div>
          <div class="book__page"></div>
          <div class="book__page"></div>
        </div>
      </div>
    </div>

    <div id="projeto-video-wrapper">
      <a class="close" href="#">x</a>
      <!-- The <iframe> (and video player) will replace this <div> tag. -->
      <div id="projeto-video-player"></div>
    </body>
    </div>

    <!-- Modal contato -->
    <div class="modal fade" id="contato">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Contato</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <?php if (!empty($msg)) {
                echo "<p>$msg</p><br><br>";
            } ?>
            <form method="POST">

              <span class="input input--haruki">
                <input class="input__field input__field--haruki" type="text" name="name" id="name" placeholder="Nome" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Nome';"/>
                <label class="input__label input__label--haruki" for="name">
                  <span class="input__label-content input__label-content--haruki">Nome</span>
                </label>
              </span>
              <span class="input input--haruki">
                <input class="input__field input__field--haruki" type="text" name="email" id="email" placeholder="Email" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Email';"/>
                <label class="input__label input__label--haruki" for="email">
                  <span class="input__label-content input__label-content--haruki">Email</span>
                </label>
              </span>
              <span class="input input--haruki">
                <textarea class="input__field input__field--haruki" type="text" name="message" id="message" rows="4" cols="50" placeholder="Mensagem" style="margin-top:16px;height:125px;text-align:left" onfocus="this.placeholder = '';" onblur="this.placeholder = 'Mensagem';"></textarea>
                <label class="input__label input__label--haruki label-high textarea" style="height:125px;text-align:left" for="message">
                  <span class="input__label-content input__label-content--haruki">Mensagem</span>
                </label>
              </span>
              <br>
              <input type="submit" value="Enviar" class="btn">
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Navegação -->
    <nav class="navbar navbar-toggleable-md">
      <button class="navbar-toggler navbar-toggler-right hamburger hamburger--stand" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>
      <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item module inverted">
            <a class="nav-link .underline--magical projeto-video" href="#" data-embed="-Wy2ja0h3Uo">o projeto</a>
          </li>
          <li class="nav-item module inverted">
            <a class="nav-link" href="#depoimentos">conheça as histórias</a>
          </li>
          <li class="nav-item module inverted">
            <a class="nav-link" href="#livros">baixe os livros</a>
          </li>
          <li class="nav-item module inverted">
            <a class="nav-link" href="#repercussao">repercussão</a>
          </li>
          <li class="nav-item module inverted">
            <a class="nav-link" href="http://www.bancossociais.org.br/Hotsite/37/Banco-de-Livros/pt/Pagina/533/" target="_blank">doe livros</a>
          </li>
          <li class="nav-item module inverted">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#contato">contato</a>
          </li>
          <div class="underbar"></div>
        </ul>
      </div>
    </nav>

    <!-- Abertura -->
    <section id="section-top" class="hero text-center fluid-container">
      <video controls id="video-bg" autoplay repeat loop>
        <source id="video-source" src="video/top_bg.mp4" type="video/mp4"></source>
      </video>
      <div class="container">
        <div class="title module fixedpos">
          <img src="img/site_title.png" width="557" height="244" alt="Prefácio de Histórias: começos de livros que contam recomeços de vidas.">
        </div>
      </div>
      <a href="#" class="video-cta block projeto-video" data-embed="-Wy2ja0h3Uo">
        <img src="img/i_play.png" width="40" height="40" alt="">
        <span class="module">&nbsp;Assista e conheça o projeto</span>
      </a>
      <a href="#depoimentos" alt="Conheça as histórias" class="scroll-arrow module inverted">
        <img src="img/i_down.png" width="26" height="15" class="pulse">
      </a>
    </section>

    <div id="content" class="container">
      <!-- Depoimentos -->
      <section id="depoimentos" class="hero text-center">
        <div class="container">
          <h2 class="module inverted">A HISTÓRIA POR TRÁS DAS PÁGINAS</h2>
          <div class="row">
            <div class="col-lg-6">
              <div class="video-container youtube" data-embed="CkOzARfua0Q">
                <div class="video-thumb video-thumb-2 align-middle">
                  <img src="img/i_play_lg.png" width="94" height="94" class="play-button align-middle">
                  <div class="video-thumb-overlay"></div>
                  <p class="module">Rodrigo</p>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="video-container youtube" data-embed="KcX3nQseBXk">
                <div class="video-thumb video-thumb-2 align-middle">
                  <img src="img/i_play_lg.png" width="94" height="94" class="play-button align-middle">
                  <div class="video-thumb-overlay"></div>
                  <p class="module">Marcos</p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="video-container youtube" data-embed="0-KBhjQLfBk">
                <div class="video-thumb video-thumb-2 align-middle">
                  <img src="img/i_play_lg.png" width="94" height="94" class="play-button align-middle">
                  <div class="video-thumb-overlay"></div>
                  <p class="module">Gilvano</p>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="video-container youtube" data-embed="Dc_xZUfj8gs">
                <div class="video-thumb video-thumb-2 align-middle">
                  <img src="img/i_play_lg.png" width="94" height="94" class="play-button align-middle">
                  <div class="video-thumb-overlay"></div>
                  <p class="module">Érico</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <hr>

      <!-- Baixe os Livros -->
      <section id="livros" class="hero text-center">
        <div class="container">
          <h2 class="module inverted">Baixe os livros</h2>
          <div class="row owl-livros">

            <div class="col-md-4 module">
              <div class="book-frame">
                <img src="img/lv_quixote.jpg" width="206" height="317" alt="">
              </div>
                <p class="author">Prefácio de Erico</p>
                <a href="livros/EBOOK_Dom_Quixote.pdf" target="_blank" alt="Baixar" class="btn module">Baixar</a>
            </div>

            <div class="col-md-4 module">
              <div class="book-frame">
                <img src="img/lv_romeujulieta.jpg" width="206" height="317" alt="">
              </div>
              <p class="author">Prefácio de Cláudio</p>
              <a href="livros/EBOOK_Romeu_e_Julieta.pdf" target="_blank" alt="Baixar" class="btn module">Baixar</a>
            </div>

            <div class="col-md-4 module">
              <div class="book-frame">
                <img src="img/lv_casmurro.jpg" width="206" height="317" alt="">
              </div>
              <p class="author">Prefácio de Rodrigo</p>
              <a href="livros/EBOOK_Dom_Casmurro_v_RODRIGO.pdf" target="_blank" alt="Baixar" class="btn module">Baixar</a>
              <p class="author">Prefácio de Denys</p>
              <a href="livros/EBOOK_Dom_Casmurro_v_DENYS.pdf" target="_blank" alt="Baixar" class="btn module">Baixar</a>
            </div>
          </div>

          <div class="row owl-livros">

            <div class="col-md-4 module">
              <div class="book-frame">
                <img src="img/lv_lear.jpg" width="206" height="317" alt="">
              </div>
              <p class="author">Prefácio de Mayara</p>
              <a href="livros/EBOOK_Rei_Lear.pdf" target="_blank" alt="Baixar" class="btn module">Baixar</a>
            </div>

            <div class="col-md-4 module">
              <div class="book-frame">
                <img src="img/lv_pessoa.jpg" width="206" height="317" alt="">
              </div>
              <p class="author">Prefácio de Gilvano</p>
              <a href="livros/EBOOK_Poemas_Fernando Pessoa.pdf" target="_blank" alt="Baixar" class="btn module">Baixar</a>
            </div>

            <div class="col-md-4 module">
              <div class="book-frame">
                <img src="img/lv_policarpo.jpg" width="206" height="317" alt="">
              </div>
              <p class="author">Prefácio de Marcos A.</p>
              <a href="livros/EBOOK_Triste_Fim_v_MARCOS.pdf" target="_blank" alt="Baixar" class="btn module">Baixar</a>
              <p class="author">Prefácio de Paulo E.</p>
              <a href="livros/EBOOK_Triste_Fim_v_PAULO.pdf" target="_blank" alt="Baixar" class="btn module">Baixar</a>
            </div>

          </div>
        </div>
      </section>
      <hr>

      <!-- Repercussão -->
      <section id="repercussao" class="hero text-center">
        <div class="container">
          <h2 class="module inverted">Repercussão</h2>
          <div id="owl-midia" class="owl-carousel owl-theme block">
            <div class="item module">
             <div href="img/midia_1.jpg" class="midia-foto gallery" style="background-image: url('img/midia_1.jpg');" data-toggle="lightbox" data-gallery="galeria-repercussao" data-type="image"></div>
            </div>
            <div class="item module">
             <div href="img/midia_2.jpg" class="midia-foto gallery" style="background-image: url('img/midia_2.jpg');" data-toggle="lightbox" data-gallery="galeria-repercussao" data-type="image"></div>
            </div>
            <div class="item module">
             <div href="img/midia_3.jpg" class="midia-foto gallery" style="background-image: url('img/midia_3.jpg');" data-toggle="lightbox" data-gallery="galeria-repercussao" data-type="image"></div>
            </div>
          </div>
        </div>
      </section>
      <hr>

      <!-- Rodapé -->
      <footer class="text-center">
        <h2 class="module inverted">Doe livros. Doe transformação.</h2>
        <p class="module">
          <a href="http://www.bancossociais.org.br/Hotsite/37/Banco-de-Livros/pt/Pagina/533/">Clique aqui</a>,
          saiba como doar e conheça<br>
          mais sobre o trabalho do banco de livros.
        </p>
        <div style="height: 55px"></div>
        <a href="http://www.bancossociais.org.br/" target="_blank" class="no-underline">
          <img class="module fixedpos" width="273" height="73" src="img/footer-logo-01.png" alt="Iniciativa: Fundação Gaúcha dos Bancos Sociais e Banco de Livros.">
        </a>
        <a href="http://www.bancossociais.org.br/Hotsite/37/Banco-de-Livros/pt/Inicial" class="no-underline" target="_blank">
          <img class="module fixedpos" width="152" height="73" src="img/footer-logo-02.png" alt="Iniciativa: Fundação Gaúcha dos Bancos Sociais e Banco de Livros.">
        </a>
      </footer>

    </div>

    <!-- jQuery, Popper.js, Bootstrap JS, Owl Carousel, Featherlight Lightbox -->
    <script src="js/bootstrap.js"></script>

    <script>
      // instanciate new modal
      /*
        var modal = new tingle.modal({
            footer: true,
            stickyFooter: false,
            closeMethods: ['overlay', 'button', 'escape'],
            closeLabel: "Fechar",
            cssClass: ['custom-class-1', 'custom-class-2'],
            beforeClose: function() {
                return true; // close the modal
            }
        });
        // set content
        modal.setContent($("#contato").html());
        */
    </script>
  </body>
</html>
